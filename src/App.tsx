import React, { useState } from "react";
import Carousel from "./components/carousel";
import styled from "styled-components";
import SlideContext from "./contexts/slidecontext";
import SlideButtons from "./components/slidebuttons";
import AboutDark from "./assets/img/image-about-dark.jpg";
import AboutLight from "./assets/img/image-about-light.jpg";
import SlideSetup from "./assets/slideSetup";
import { ReactComponent as ShopArrow } from "./assets/img/icon-arrow.svg";
import NavBar from "./components/NavBar";

function App() {
  const [slide, setSlide] = useState(0);
  const [fill, setFill] = useState("#000");

  const ShopLink = styled.a`
    letter-spacing: 0.5em;
  `;

  const AboutTitle = styled.h1`
    margin-top: 12.5%;
    margin-left: 10%;
  `;

  return (
    <div className="App h-full">
      <SlideContext.Provider value={{ slide, setSlide, slides: SlideSetup }}>
        <div className="grid grid-rows-3 h-full">
          <div className="row-span-2">
            <div className="grid grid-cols-10 grid-rows-1 gap-0 h-full">
              <div className="col-span-6 bg-black">
                <NavBar />
                <Carousel />
              </div>
              <div className="col-span-4 bg-white relative">
                <h1 className="text-4xl font-semibold mx-16 mt-16">
                  {SlideSetup[slide].title}
                </h1>
                <h3 className="font-thin text-gray mt-8 mx-16 mb-8 text-xs tracking-wider">
                  {SlideSetup[slide].text}
                </h3>
                <div>
                  <ShopLink
                    className="mx-16 betterhover:hover:text-gray"
                    onMouseEnter={() => setFill("hsl(0, 0%, 63%")}
                    onMouseLeave={() => setFill("#000")}
                    href="#"
                  >
                    <h1 className="inline-block align-middle">SHOP NOW</h1>
                    <div className="inline-block">
                      <ShopArrow fill={fill} className="inline-block ml-4 h-full" />
                    </div>
                  </ShopLink>
                </div>
                <div className="bottom-0 absolute w-1/4" id="buttonContainer">
                  <SlideButtons numberOfSlides={SlideSetup.length} />
                </div>
              </div>
            </div>
          </div>
          <div className="row-span-1 overflow-hidden">
            <div className="grid grid-cols-10 h-full">
              <div className="col-span-3 bg-black">
                <img
                  src={AboutDark}
                  className="w-full h-full"
                  alt="Coffee table in darker room"
                />
              </div>
              <div className="col-span-4 bg-white">
                <AboutTitle className="font-bold tracking-widest">
                  ABOUT OUR FURNITURE
                </AboutTitle>
                <p
                  style={{ marginLeft: "10%", marginRight: "10%" }}
                  className="text-xs text-gray mt-4"
                >
                  Our multifunctional collection blends design and function to
                  suit your individual taste. Make each room unique, or pick a
                  cohesive theme that best express your interests and what
                  inspires you. Find the furniture pieces you need, from
                  traditional to contemporary styles or anything in between.
                  Product specialists are available to help you create your
                  dream space.
                </p>
              </div>
              <div className="col-span-3 bg-black">
                <img
                  src={AboutLight}
                  className="w-full h-full"
                  alt="White stool on light background"
                />
              </div>
            </div>
          </div>
        </div>
      </SlideContext.Provider>
    </div>
  );
}

export default App;
