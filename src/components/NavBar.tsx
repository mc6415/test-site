import React, { useState } from "react";
import styled from "styled-components";
import Logo from "../assets/img/logo.svg"

function NavBar() {
  let [active, setActive] = useState("home");

  let PageLink = styled.a`
    position: relative;

    &:hover {
      &:after {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        content: "";
        border-bottom: 2px white solid;
        width: 50%;
        text-align: center;
        margin: 0 auto;

      }
    }
  `;

  let pages = ["home", "shop", "about", "contact"];

  let pageList = pages.map((page) => (
      <PageLink className="pb-2 mr-8 text-white" href="#" onClick={() => setActive(page)}>{page}</PageLink>
  ));

  return (
    <div className="absolute mt-10 ml-10">
      <img src={Logo} className="inline mr-16" alt="room" />
      <div className="mt-10 inline bottom-1">
        {pageList}
      </div>
    </div>
  );
}

export default NavBar;
