import React, { useContext } from "react";
import SlideContext from "../contexts/slidecontext";

const Carousel = () => {
  const { slide, slides } = useContext(SlideContext);

  return (
    <div className="h-full bg-cover">
      <img src={slides[slide].image} className="h-full w-full" alt="Hero" />
    </div>
  );
};

export default Carousel;
 