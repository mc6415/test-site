import React, { useContext } from "react";
import styled from "styled-components";
import SlideContext from "../contexts/slidecontext";
import AngleLeft from "../assets/img/icon-angle-left.svg";
import AngleRight from "../assets/img/icon-angle-right.svg";


type ButtonProps = {
  numberOfSlides: number;
};

const CarouselButton = styled.button`
  border-radius: 0px;
  color: white;
  padding: 20px;
  width: 50%;
`;

const SlideButtons = ({ numberOfSlides }: ButtonProps) => {
  const { slide, setSlide } = useContext(SlideContext);

  function nextSlide() {
    if (slide + 1 === numberOfSlides) {
      setSlide(0);
  } else {
      setSlide(slide + 1);
    }
  }

  function previousSlide() {
    if (slide === 0) {
      setSlide(numberOfSlides - 1);
    } else {
      setSlide(slide - 1);
    }
  }

  return (
    <div>
      <CarouselButton
        onClick={() => previousSlide()}
        className="pointer-events-auto bg-black betterhover:hover:bg-darkgray font-medium focus:outline-none"
      >
        <img width="75%" src={AngleLeft} alt="Left arrow" />
      </CarouselButton>
      <CarouselButton
        onClick={() => nextSlide()}
        className="bg-black betterhover:hover:bg-darkgray font-medium focus:outline-none"
      >
        <img width="75%" src={AngleRight} alt="Right Arrow" />
      </CarouselButton>
    </div>
  );
};

export default SlideButtons;
