import React from 'react';

interface slideContext {
  slide: number,
  slides: Array<any>,
  setSlide: any,
}

let context: slideContext = {
  slide: 0,
  slides: [],
  setSlide: null,
}

const SlideContext = React.createContext(context);

export default SlideContext;