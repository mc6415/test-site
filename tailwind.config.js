module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      display: ["Spartan"],
      body: ["Spartan"],
    },
    colors: {
      gray: "hsl(0, 0%, 63%)",
      black: "hsl(0, 0%, 0%)",
      white: "hsl(0, 0%, 100%)",
      darkgray: "hsl(0, 0%, 27%)",
    },
    screens: {
      mobile: "375px",
      desktop: "1440px",
    },
    extend: {
      screens: {
        "betterhover": { "raw": "(hover: hover)" },
      },
    },
  },
  variants: {
    extend: {
      padding: ["hover", "focus"],
    },
  },
  plugins: [],
};
